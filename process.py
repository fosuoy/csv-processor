#!/usr/bin/env python2
#
# A simple script to store the data within CSV files in the 'common directory'
# directory then process all data as described in the README.md file

from csv                    import DictReader
from datetime               import datetime, timedelta
from fnmatch                import fnmatch
from os                     import path, walk
from pandas_datareader.data import DataReader
from sys                    import exit
from sqlite3                import connect, Error

def read_csv_into_db(file_obj):
  # Read a csv file into trades.db
  # Output messages as trades are 'executed' / display which are rejected
  messages = []
  errors   = []
  try:
    con = connect('trades.db')
    cur = con.cursor()
  except Error, e:   
    print "Error %s:" % e.args[0]
    exit(1)

  reader = DictReader(file_obj, delimiter=',')
  for row in reader:
    missing         =                           None
    instrument      = row['Instrument']      or None
    price           = row['Price']           or None
    quantity        = row['Quantity']        or None
    timestamp       = row['Timestamp']       or None
    tradereference  = row['TradeReference']  or None
    instrumenttype  = row['InstrumentType']  or None
    underlyingasset = row['UnderlyingAsset'] or None
    clientreference = row['ClientReference'] or None
    if instrument == None: missing = "instrument"
    if price      == None: missing = "price"
    if quantity   == None: missing = "quantity"
    if timestamp  == None: missing = "timestamp"
    if missing: 
      errors.append( "Mandatory information ( {} ) is missing from trade in file: {}, skipping trade".\
                                                                      format(missing, file_obj.name) )
      continue
    sqlQuery = """INSERT INTO trades (
                    Instrument,
                    Price,
                    Quantity,
                    Timestamp,
                    TradeReference,
                    InstrumentType,
                    UnderlyingAsset,
                    ClientReference )
                  VALUES (
                    '{}','{}','{}','{}','{}','{}','{}','{}' )""".format(instrument,
                                                                    price,
                                                                    quantity,
                                                                    timestamp,
                                                                    tradereference,
                                                                    instrumenttype,
                                                                    underlyingasset,
                                                                    clientreference)
    cur.execute(sqlQuery)
    messages.append("Trade executed: exchanged {} units of {} at {}. Valued at: {}".format(quantity,
                                                                                           instrument,
                                                                                           price,
                                                                                           float(price) * int(quantity) ) )
  con.commit()
  con.close()
  return messages, errors

def read_instrument_pricing_into_db():
  # Get list of instruments, 
  Instruments = run_db_query('select distinct(Instrument), Timestamp from trades;')
  for instrument in Instruments:
    instrument_date = instrument[1].split("-")
    deltatime = timedelta(days=-1)
    start = datetime(int(instrument_date[0]),
                              int(instrument_date[1]),
                              int(instrument_date[2]))
    end   = start - deltatime
    data = DataReader(instrument[0], "yahoo", start, end)
    MarketValue  = data['Volume'] * data['Adj Close']
    ClosingPrice = data['Adj Close']
    AveragePrice = (data['High'] + data['Low']) / 2
    sqlQuery = """INSERT INTO stocks (
                    Instrument,
                    Timestamp,
                    MarketValue,
                    ClosingValue,
                    Average )
                  VALUES ( '{}', '{}','{}','{}','{}')""".format(instrument[0],
                                                                instrument[1],
                                                               MarketValue[1],
                                                              ClosingPrice[1],
                                                              AveragePrice[1])
    run_db_query(sqlQuery, True)

def get_csv_files_in_common():
  # Get list of CSV files in common_directory/
  csvFiles = []
  for directorypath, directoryname, filenames in walk('./common_directory'):
    for ff in filenames:
      if fnmatch(ff, '*.csv'):
        csvFiles.append(path.join(directorypath, ff))
  return csvFiles

def init_db():
  # Create database schem if not present in directory
  con = None
  if not path.isfile('trades.db'):
    tradesTable = """CREATE TABLE trades (
                       Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                       Instrument TEXT NOT NULL,
                       Price REAL NOT NULL,
                       Quantity INT NOT NULL,
                       Timestamp TIMESTAMP NOT NULL,
                       TradeReference INT,
                       InstrumentType TEXT,
                       UnderlyingAsset TEXT,
                       ClientReference INT)"""
    stocksTable = """CREATE TABLE stocks (
                       Id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                       Instrument TEXT NOT NULL,
                       Timestamp TIMESTAMP NOT NULL,
                       MarketValue REAL NOT NULL,
                       ClosingValue REAL NOT NULL,
                       Average REAL NOT NULL)"""
    run_db_query(tradesTable, True)
    run_db_query(stocksTable, True)

def generate_report():
  # Generate a list of messages / report from info in DB here
  messages = []

  # From a trade reference previous trades can be found (Ignore trades without a reference)
  TradeReferences = run_db_query('select distinct(TradeReference) from trades;')
  for TradeReference in TradeReferences:
    if TradeReference[0] != "None":
      yearMonth = datetime.now().strftime("%Y-%m-")
      sqlQuery = """SELECT COUNT(Id)
                    FROM trades
                    WHERE tradereference={} AND timestamp like '{}%';""".format(TradeReference[0],
                                                                                 yearMonth)
      numberOfPreviousTrades = run_db_query(sqlQuery)
      messages.append("The customer with trade reference number: {} has made {} previous trades this month.".\
                                                     format(TradeReference[0], numberOfPreviousTrades[0][0]) )
  # Add line for formatting purposes
  messages.append('#' * 80)
  # Show that historical data can be pulled out both real and what is in the DB
  Instruments = run_db_query('select distinct(instrument) from trades;')
  for instrument in Instruments:
    sqlQuery = """SELECT Timestamp, SUM(Price * Quantity), Avg(Price)
                  FROM trades
                  WHERE instrument='{}'
                  GROUP BY timestamp;""".format(instrument[0])
    TradeVolume = run_db_query(sqlQuery)
    for volume in TradeVolume:
      messages.append("On date: {} - {} units of {} were processed at an average: {} currency units.".\
                                     format(volume[0], volume[1], instrument[0], round(volume[2], 2)) )
      sqlQuery = """SELECT ClosingValue, Average
                    FROM stocks
                    WHERE instrument='{}'
                    AND timestamp='{}';""".format(instrument[0], volume[0])
      MarketValue = run_db_query(sqlQuery)
      messages.append("On this date the stock closed at: {} with an average value of: {}".\
                                             format(MarketValue[0][0], MarketValue[0][1]) )
      messages.append('#' * 80)
  return messages

def run_db_query(sqlQuery, commit=False):
  # Not good if running a lot of queries, used here as a PoC
  # open db connection, run query then close db connection, I realize it's
  # inefficient to open / close connections to the db so many times
  try:
    con = connect('trades.db')
    cur = con.cursor()    
    data = cur.execute( sqlQuery )
    data = data.fetchall()
  except Error, e:   
    print "Error %s:" % e.args[0]
    exit(1)
  finally:    
    if con:
      if commit:
        con.commit()
      con.close()
  return data

def separator():
  print '#' * 80

if __name__ == "__main__":
# Start up DB and read CSV files into it
  init_db()
  csv_files_to_process = get_csv_files_in_common()
  for csv_file in csv_files_to_process:
    with open(csv_file, "rb") as f_obj:
      # Output errors after trades
      trades, errors = read_csv_into_db(f_obj)
      for trade in trades:
        print trade
      for error in errors:
        print error
  # Make sure data is present about instruments for timestamped days of trading
  read_instrument_pricing_into_db()
  # Generate a report based on what is currently in DB
  messages = generate_report()
  for message in messages:
    print message
