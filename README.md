# CSV-Processing Script
This is a simple script to process data given in a CSV, send it to an sqlite DB
then process that information and output a report.

Run this a few times with the test data CSVs / try altering them  to see changing reports.

## Assumptions

CSV files received have headers indicating the following:

**Instrument**, **price**,**quantity**,**timestamp**,trade reference,
instrument type, underlying asset and client reference

Where items in bold are mandatory (files without headers indicating items in bold
are rejected) and timestamps are standardized in the format:
YEAR-MONTH-DAY

## Requirements
A working internet connection, during runtime this does fetch some data from the internet.

sqlite - available as a package from most distribution's package managers e.g.:
`apt-get install -y sqlite`
as a privileged user.

This also requires some packages from pip, there is a requirements file so either:
Create a virtual python environment:

`virtualenv env`

`source env/bin/activate`

Or skip straight here if you'd like to install the packages directly to your
computer.

`pip install -r requirements.txt`

Then while still in your virtual environment you can run:
`./process.py`


## What this does
For a given set of CSV files this will process:
- Market value of each trade made
- Total market value of instrument, closing value and avg. price / day.
- For each trade reference the constituent trades are given.
- For each day we need total traded value, closing value and the closing position

## Where does this produce reports
Reports will be left in your console / terminal.
Additionally an sqlite database will be left in this directory title:
trades.db
